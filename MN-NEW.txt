Filename:MN-NEW
ClrText
"Huomio! Uuden  pelin aloittaminen poistaa vanhan tallenteen."[DISP]
Lbl 2
ClrText
"Uusi peli muistipaik-kaan..?              [1-6] Muistipaikat   0 Ei uutta peliä"
?->A
If A=0
Then Return
IfEnd
If A>0 And A<7
Then A->S
Goto 1
IfEnd
Goto 2
Lbl 1
0->L
Do
L+1->L
0->Mat M[L,S]
LpWhile L<11
Prog "MN-GAMEC"
Return