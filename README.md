# Old CASIO programs

This is a collection of old programs for the CASIO fx-9860g that I wrote when I was bored back in school. Don't ask me how they work, I have no idea. It contains programs:

1. 7-Ventti (`7-*.txt`)
2. Dice (`DICE*.txt`)
3. Lollercoaster (`LOLLER.txt`)
4. Matrix screensaver (`MATRIX.txt` and `MTRXCLR.txt`)
5. "The Mine" text game (`MN-*.txt`)
6. "Pascal" (no idea what it is) (`PASCAL.txt`)
7. Reaction game (`REAC*.txt`)
8. Battleship (`SINK*.txt`)
9. Utility programs used by other programs (`S-*.txt`)

Special characters have been replaced by roughly equivalent ASCII characters. For example, `→` into `->` and `⊿` into `[DISP]`. This means that the programs cannot be loaded directly onto a calculator, they would need to be typed manually. The code here is only for reference.

## Licence

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                        Version 2, December 2004 

     Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

     Everyone is permitted to copy and distribute verbatim or modified 
     copies of this license document, and changing it is allowed as long 
     as the name is changed. 

                DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

      0. You just DO WHAT THE FUCK YOU WANT TO.
