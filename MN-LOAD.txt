Filename:MN-LOAD
Lbl 1
ClrText
"Valitse muistipaikka.[1-6] Muistipaikat   0 Takaisin"
?->A
If A=0
Then Return
IfEnd
If A>0 And A<7
Then A->S
If Mat M[11,S]=1
Then Prog "MN-GAMEC"
Else ClrText
"Tässä muistipaikassa ei ole tallennettua  peliä."[DISP]
Goto 1
IfEnd
Return
IfEnd
Goto 1