Filename:MN-LVLWO
Lbl 2
ClrText
"Tee töitä hakkaamallanappeja. Saat rahaa  sen mukaan kuinka    paljon jaksat napu-  tella tietyssä aika- määrässä."[DISP]
ClrText
"Pitämällä nappia poh-jassa ei voi huijata;joka kerta pitää pai-naa eri nappia. Aika lähtee heti, ole val-miina!"[DISP]
ClrText
0->R
0->L
Do
Getkey->A
If A=B Or A=0
Then Goto 1
IfEnd
A->B
R+1->R
Locate 1,1,R
Lbl 1
L+1->L
LpWhile L<2000
R/10->R
Int (R)->R
Mat M[10,S]+R->Mat M[10,S]
Lbl 3
ClrText
"Tienasit:            1. Pelaa uudestaan   2. Pois!"
Locate 11,1,R
?->A
If A=1
Then Goto 2
IfEnd
If A=2
Then Return
IfEnd
Goto 3