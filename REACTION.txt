Filename:REACTION
">>>>>>REACTION<<<<<<   1    2    3    +    [ ]  [ ]  [ ]  [ ]                        T:        N:         H:        G:         [EXE]#E690/#E691[EXIT]   "
Lbl 1
0.1->L
Prog "S-TIMER"
Locate 7,1,"REACTION"
Mat D[1,2]->H
Mat D[1,1]->G
Mat D[1,3]->T
Locate 4,6,H
Locate 14,6,G
Locate 4,5,T
Prog "S-GK"
If Z=31
Then Prog "REAC-NEW"
Goto 1
IfEnd
If Z=47
Then ClrText
Stop
IfEnd
Goto 1